//
//  LevelCell.swift
//  HSNBA
//
//  Created by leobs on 07.04.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import UIKit

class LevelCell: UICollectionViewCell {
    
    let twLevelNumber : UILabel = {
        let twLevelNumber = UILabel()
        twLevelNumber.translatesAutoresizingMaskIntoConstraints = false
        twLevelNumber.text = "1"
        twLevelNumber.backgroundColor = .clear
        twLevelNumber.font = UIFont.boldSystemFont(ofSize: 23)
        twLevelNumber.textColor = .black
        twLevelNumber.textAlignment = .left
        return twLevelNumber
    }()
    
    let twPlaySign : UILabel = {
        let twPlaySign = UILabel()
        twPlaySign.translatesAutoresizingMaskIntoConstraints = false
        twPlaySign.text = ""
        twPlaySign.backgroundColor = .clear
        twPlaySign.font = UIFont.boldSystemFont(ofSize: 15)
        twPlaySign.textColor = .black
        twPlaySign.textAlignment = .center
        return twPlaySign
    }()
    
    let iwLevelSign: UIImageView = {
        let iwLevelSign = UIImageView(image: #imageLiteral(resourceName: "ic_green_tick"))
        iwLevelSign.translatesAutoresizingMaskIntoConstraints = false
        return iwLevelSign
    }()
    
    let iwLevelLogo: UIImageView = {
        let iwLevelLogo = UIImageView(image:#imageLiteral(resourceName: "league_eng"))
        iwLevelLogo.translatesAutoresizingMaskIntoConstraints = false
        return iwLevelLogo
    }()
    
    let iwCover : UIView = {
        let iwCover = UIView()
        iwCover.translatesAutoresizingMaskIntoConstraints = false
        iwCover.backgroundColor = UIColor.black
        iwCover.alpha = 0.5
        return iwCover
    }()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        backgroundColor = UIColor(red:247/255, green: 246/255, blue: 183/255,alpha: 1.0)
        self.addSubview(twLevelNumber)
        twLevelNumber.widthAnchor.constraint(equalToConstant: self.frame.size.width).isActive = true
        twLevelNumber.leftAnchor.constraint(equalTo: self.leftAnchor, constant: self.frame.size.width/20).isActive = true
        twLevelNumber.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        twLevelNumber.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        self.addSubview(twPlaySign)
        twPlaySign.widthAnchor.constraint(equalToConstant: self.frame.size.width).isActive = true
        twPlaySign.leftAnchor.constraint(equalTo: self.leftAnchor, constant: self.frame.size.width/20).isActive = true
        twPlaySign.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        twPlaySign.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        self.addSubview(iwLevelSign)
        iwLevelSign.widthAnchor.constraint(equalToConstant: self.frame.size.width/5).isActive = true
        iwLevelSign.heightAnchor.constraint(equalToConstant: self.frame.size.width/5).isActive = true
        iwLevelSign.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        iwLevelSign.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        self.addSubview(iwLevelLogo)
        iwLevelLogo.widthAnchor.constraint(equalToConstant: (self.frame.size.width*2)/3).isActive = true
        iwLevelLogo.heightAnchor.constraint(equalToConstant: (self.frame.size.width*2)/3).isActive = true
        iwLevelLogo.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        iwLevelLogo.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        self.addSubview(iwCover)
        iwCover.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        iwCover.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        iwCover.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        iwCover.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        
    }

}
