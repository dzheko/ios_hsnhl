//
//
// DAO Object for Player
//  @author Dzh
//  @since 06.03.2018
//

import Foundation

class PlayerDAO{
    
    let CATEGORY1_ = "SHOOTING_POINTS_"
    let CATEGORY2_ = "SHOOTING_PENALTY_MINUTES_"
    let CATEGORY3_ = "SHOOTING_SHOTS_ON_GOAL_"
    let CATEGORY4_ = "SHOOTING_PERCENTAGE_"
    let CATEGORY5_ = "SHOOTING_SHOTS_PER_GAME_"
    let CATEGORY6_ = "SHOOTING_GOALS_PER_GAME_"
    let CATEGORY7_ = "SHOOTING_ASSISTS_PER_GAME_"
   
    init(){}
    
    /**
     * Creates Player
     */
    public func getPlayerData(categoryToPlayNumber : Int) -> Player {
        
        let randomNumber =  arc4random_uniform(140) + 0
    
        let PLAYER_TEAM = "SHOOTING_TEAM_NAME_" + String(randomNumber)
        let PLAYER_NAME = "SHOOTING_PLAYER_NAME_" + String(randomNumber)
        
        let CATEGORY1 =  CATEGORY1_ + String(randomNumber)
        let CATEGORY2 =  CATEGORY2_ + String(randomNumber)
        let CATEGORY3 =  CATEGORY3_ + String(randomNumber)
        let CATEGORY4 =  CATEGORY4_ + String(randomNumber)
        let CATEGORY5 =  CATEGORY5_ + String(randomNumber)
        let CATEGORY6 =  CATEGORY6_ + String(randomNumber)
        let CATEGORY7 =  CATEGORY7_ + String(randomNumber)
    
        let playerTeam = NSLocalizedString(PLAYER_TEAM, comment: "")
        let playerName = NSLocalizedString(PLAYER_NAME, comment: "")
        let category1Stat = Double(NSLocalizedString(CATEGORY1, comment: ""))
        let category2Stat = Double(NSLocalizedString(CATEGORY2, comment: ""))
        let category3Stat = Double(NSLocalizedString(CATEGORY3, comment: ""))
        let category4Stat = Double(NSLocalizedString(CATEGORY4, comment: ""))
        let category5Stat = Double(NSLocalizedString(CATEGORY5, comment: ""))
        let category6Stat = Double(NSLocalizedString(CATEGORY6, comment: ""))
        let category7Stat = Double(NSLocalizedString(CATEGORY7, comment: ""))
        
        let player = Player(playerName: playerName, teamName : playerTeam)
        
        switch categoryToPlayNumber {
            case 1:  player.playerCategoryStatToPlay = category1Stat; break;
            case 2:  player.playerCategoryStatToPlay = category2Stat; break;
            case 3:  player.playerCategoryStatToPlay = category3Stat; break;
            case 4:  player.playerCategoryStatToPlay = category4Stat; break;
            case 5:  player.playerCategoryStatToPlay = category5Stat; break;
            case 6:  player.playerCategoryStatToPlay = category6Stat; break;
            case 7:  player.playerCategoryStatToPlay = category7Stat; break;
            default: player.playerCategoryStatToPlay = 0; break;
        }
        
        return player
    }
    
}
