//
//  AnalyticsViewController.swift
//  HSNBA
//
//  Created by leobs on 10.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class AnalyticsViewController:UIViewController{
    
    var tracker: GAITracker? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initilializeGoogleAnalytics()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initilializeGoogleAnalytics(){
        let gai = GAI.sharedInstance()
        tracker = gai?.tracker(withTrackingId: IdSettings.GOOGLE_ANALYTICS_ID)
        tracker?.set(kGAIScreenName, value: NSStringFromClass(self.classForCoder))
        let event = GAIDictionaryBuilder.createScreenView()
        tracker?.send(event!.build() as! [NSObject: Any])
    }
    
    func sendEvent(category: String, action: String, label: String) {
        let event = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: NSNumber(value: 120))
        tracker?.send(event!.build() as! [NSObject: Any])
    }
    
    
}
