//
//  MyTableViewCell.swift
//  HSNBA
//
//  Created by leobs on 09.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation
import UIKit

class MyTableViewCell: UITableViewCell {
    
    var twLeagueName: UILabel!
    var twCategoryName: UILabel!
    var twWorldScore : UILabel!
    var twUserScore : UILabel!
    
    
    var myButton1 : UIButton!
    var myButton2 : UIButton!
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y += 4
            frame.size.height -= 2 * 5
            super.frame = frame
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        twLeagueName = UILabel()
        twLeagueName.textColor = UIColor(red:38/255, green: 94/255, blue: 183/255,alpha: 1.0)
        twLeagueName.font = UIFont.boldSystemFont(ofSize: 25)
        twLeagueName.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(twLeagueName)
        
        twLeagueName.topAnchor.constraint(equalTo: contentView.topAnchor, constant: contentView.frame.size.height/15).isActive = true
        twLeagueName.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: contentView.frame.size.width/40).isActive = true
        twLeagueName.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        twLeagueName.heightAnchor.constraint(equalToConstant: contentView.frame.size.height).isActive = true
        
        twCategoryName = UILabel()
        twCategoryName.textColor = UIColor.black
        twCategoryName.font = UIFont.boldSystemFont(ofSize: 17)
        twCategoryName.translatesAutoresizingMaskIntoConstraints = false
        
        twWorldScore = UILabel()
        twWorldScore.font = UIFont.boldSystemFont(ofSize: 15)
        twWorldScore.translatesAutoresizingMaskIntoConstraints = false
        twWorldScore.backgroundColor = .clear
        twWorldScore.textColor = UIColor(red:15/255, green: 117/255, blue: 189/255,alpha: 1.0)
        
        contentView.addSubview(twWorldScore)
        
        twWorldScore.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -contentView.frame.size.height/5).isActive = true
        twWorldScore.leftAnchor.constraint(equalTo: twLeagueName.leftAnchor).isActive = true
        twWorldScore.widthAnchor.constraint(equalToConstant: contentView.frame.size.width/2).isActive = true
        twWorldScore.heightAnchor.constraint(equalToConstant: contentView.frame.size.height/3).isActive = true
        
        
        

       

        twUserScore = UILabel()
        twUserScore.textColor = UIColor.black
        twUserScore.font = UIFont.boldSystemFont(ofSize: 15)
        twUserScore.translatesAutoresizingMaskIntoConstraints = false
        twUserScore.textAlignment = .right
        twUserScore.backgroundColor = .clear
        twUserScore.textColor = UIColor(red:11/255, green: 148/255, blue: 76/255,alpha: 1.0)
        contentView.addSubview(twUserScore)
        
        twUserScore.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -contentView.frame.size.height/5).isActive = true
        twUserScore.leftAnchor.constraint(equalTo: twWorldScore.rightAnchor, constant: contentView.frame.size.width/25).isActive = true
        twUserScore.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -contentView.frame.size.width/25).isActive = true
        twUserScore.heightAnchor.constraint(equalToConstant: contentView.frame.size.height/2).isActive = true
        
        contentView.addSubview(twCategoryName)
        
        twCategoryName.topAnchor.constraint(equalTo: twLeagueName.bottomAnchor, constant: contentView.frame.size.height/60).isActive = true
        twCategoryName.leftAnchor.constraint(equalTo: twLeagueName.leftAnchor).isActive = true
        twCategoryName.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        twCategoryName.heightAnchor.constraint(equalToConstant: contentView.frame.size.height/2).isActive = true
        
        let leagueLogo = UIImageView()
        leagueLogo.translatesAutoresizingMaskIntoConstraints = false
        leagueLogo.image = UIImage(named: "hockey_sign")
        contentView.addSubview(leagueLogo)
        leagueLogo.widthAnchor.constraint(equalToConstant: contentView.frame.size.height*1.5).isActive = true
        leagueLogo.heightAnchor.constraint(equalToConstant: contentView.frame.size.height*1.5).isActive = true
        leagueLogo.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -contentView.frame.size.width/20).isActive = true
        leagueLogo.topAnchor.constraint(equalTo: contentView.topAnchor, constant: contentView.frame.size.height/4).isActive = true
        
        
    }
    
    
    
}
