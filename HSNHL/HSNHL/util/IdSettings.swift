//
//  IdSettings.swift
//  HSNHL
//
//  Created by leobs on 10.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class IdSettings{
    
    static var APPODEAL_ID = "6b017242df863d1d755908ccd4534b929486e0daf68be2ce"
    static var ADMOB_ID_VIDEO = "ca-app-pub-8806065804623360/2019927131"
    static var ADMOB_ID_BANNER = "ca-app-pub-8806065804623360/8997287079"
    static var GOOGLE_ANALYTICS_ID = "UA-83861504-11"
    
    //Testers 
//    static var ADMOB_ID_BANNER = "ca-app-pub-3940256099942544/2934735716"
//    static var ADMOB_ID_VIDEO = " ca-app-pub-3940256099942544/1712485313"
    
    init(){}
    
}
