//
//
//  Category Helper class
//
//  @author Dzh
//  @since 06.03.2018
//

import Foundation
class CategoryHelper{
    
    init(){}
    
    /*
     * Returns category name
     * @param category
     * @return categoryFullName
     */
    static func getCategoryFullName(category: Int)->String{
        
        var categoryName = ""
        
        switch category {
        
        case 1: categoryName = "shooting points"; break;
        case 2: categoryName = "penalty minutes"; break;
        case 3: categoryName = "shots on goal"; break;
        case 4: categoryName = "shooting percentage"; break;
        case 5: categoryName = "shots per game"; break;
        case 6: categoryName = "goals per game"; break;
        case 7: categoryName = "assists per game"; break;
            
        default: categoryName = ""; break;
        }
        
        return categoryName
    }
    
    /*
     * Returns category name with TitleCase
     * @param category
     * @return categoryFullName
     */
    static func getCategoryFullNameTitleCase(category: Int)->String{
        
        var categoryName = ""
        
        switch category {
            
        case 1: categoryName = "Shooting points"; break;
        case 2: categoryName = "Penalty minutes"; break;
        case 3: categoryName = "Shots on goal"; break;
        case 4: categoryName = "Shooting percentage"; break;
        case 5: categoryName = "Shots per game"; break;
        case 6: categoryName = "Goals per game"; break;
        case 7: categoryName = "Assists per game"; break;
            
        default: categoryName = ""; break;
        }
        
        return categoryName
    }
    
    
    /*
     * Returns category name with TitleCase
     * @param category
     * @return categoryFullName
     */
    static func getCategoryQuestion(category: Int)->String{
        
        var categoryQuestion = ""
        
        switch category {
            
        case 1: categoryQuestion = "Who has more shooting points?"; break;
        case 2: categoryQuestion = "Who has more penalty minutes?"; break;
        case 3: categoryQuestion = "Who has more shots on goal?"; break;
        case 4: categoryQuestion = "Who has higher shooting percentage?"; break;
        case 5: categoryQuestion = "Who has more shots per game?"; break;
        case 6: categoryQuestion = "Who has more goals per game?"; break;
        case 7: categoryQuestion = "Who has more assists per game?"; break;
            
        default: categoryQuestion = ""; break;
        }
        
        return categoryQuestion
    }
    
    
    
    
}
