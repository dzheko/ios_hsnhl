//
//  StartViewController.swift
//  HSNBA
//
//  @author Dzh
//  @since 08.03.2018
//

import Foundation
import UIKit
import Google

class StartViewController:AnalyticsViewController{
    
    let iwBackground : UIImageView = {
        let iwBackground = UIImageView(image: #imageLiteral(resourceName: "main_page"))
        iwBackground.translatesAutoresizingMaskIntoConstraints = false
        return iwBackground
    }()

    let playGameBtn = UIButton()
    let chooseCategoryBtn = UIButton()
    let levelGameBtn = UIButton()
    
    let iwAppLogo : UIImageView = {
        let iwAppLogo = UIImageView(image: #imageLiteral(resourceName: "app_logo"))
        iwAppLogo.translatesAutoresizingMaskIntoConstraints = false
        return iwAppLogo
    }()
    
    let twGameCategory : UILabel = {
        let twGameCategory = UILabel()
        twGameCategory.translatesAutoresizingMaskIntoConstraints = false
        twGameCategory.font = UIFont.boldSystemFont(ofSize: 25)
        twGameCategory.backgroundColor = .clear
        twGameCategory.textColor = .white
        twGameCategory.textAlignment = .center
        return twGameCategory
    }()
    
    let twGameQuestion : UILabel = {
        let twGameQuestion = UILabel()
        twGameQuestion.translatesAutoresizingMaskIntoConstraints = false
        twGameQuestion.font = UIFont.boldSystemFont(ofSize: 18)
        twGameQuestion.backgroundColor = .clear
        twGameQuestion.textColor = .white
        twGameQuestion.textAlignment = .center
        return twGameQuestion
    }()
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(iwBackground)
        view.addSubview(iwAppLogo)
        view.addSubview(playGameBtn)
        view.addSubview(twGameCategory)
        view.addSubview(twGameQuestion)
        view.addSubview(chooseCategoryBtn)
        view.addSubview(levelGameBtn)
        
        createUiLayout()
        createPlayBtn()
        createLevelGameBtn()
        createCategoryBtn()
    }
    
   
    @objc func playGameBtnClicked(_ sender: UITapGestureRecognizer) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        ViewController.currentScore = 0
        sendEvent(category: Strings.MENU_ITEMS , action: "PLAY GAME BTN", label: "PLAY GAME BTN")
        SharedPreferences.setStringValueForKey(key: Strings.CURRENT_GAME_MODE, value: Strings.DEFAULT_GAME_MODE)
        self.present(viewController, animated: true, completion: nil)
    }
    
    @objc func categoryBtnClicked(_ sender: UITapGestureRecognizer) {
        let categoryViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        sendEvent(category: Strings.MENU_ITEMS , action: "CATEGORY BTN", label: "CATEGORY BTN")
        self.present(categoryViewController, animated: true, completion: nil)
    }
    
    @objc func levelGameBtnClicked(_ sender: UITapGestureRecognizer) {
        print("Entered to it")
        let levelGameViewController = self.storyboard?.instantiateViewController(withIdentifier: "LevelGameViewController") as! LevelGameViewController
        sendEvent(category: Strings.MENU_ITEMS , action: "LEVELS GAME BTN", label: "LEVELS GAME BTN")
        self.present(levelGameViewController, animated: true, completion: nil)
    }
    
    /*
     * Creates Play Button
     */
    private func createPlayBtn(){
        
        playGameBtn.translatesAutoresizingMaskIntoConstraints = false
        playGameBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        playGameBtn.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 1.5).isActive = true
        playGameBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 10).isActive = true
        playGameBtn.topAnchor.constraint(equalTo: twGameQuestion.bottomAnchor,constant: view.frame.size.height/20).isActive = true
        playGameBtn.backgroundColor = .clear
        playGameBtn.layer.cornerRadius = 10
        playGameBtn.layer.borderWidth = 2
        playGameBtn.layer.borderColor = UIColor.white.cgColor

        
        let playSign = UIImageView(image:#imageLiteral(resourceName: "play_sign"))
        playGameBtn.addSubview(playSign)
        playSign.translatesAutoresizingMaskIntoConstraints = false
        playSign.leftAnchor.constraint(equalTo: playGameBtn.leftAnchor, constant: self.view.frame.size.width / 24).isActive = true
        playSign.widthAnchor.constraint(equalToConstant:self.view.frame.size.height  / 26).isActive = true
        playSign.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 24).isActive = true
        playSign.centerYAnchor.constraint(equalTo: playGameBtn.centerYAnchor,constant: 0).isActive = true

        let twPlayGame = UILabel()
        twPlayGame.text = "PLAY GAME"
        playGameBtn.addSubview(twPlayGame)

        twPlayGame.translatesAutoresizingMaskIntoConstraints = false
        twPlayGame.leftAnchor.constraint(equalTo: playSign.rightAnchor, constant: self.view.frame.size.width / 15).isActive = true
        twPlayGame.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2).isActive = true
        twPlayGame.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twPlayGame.centerYAnchor.constraint(equalTo: playSign.centerYAnchor,constant: 0).isActive = true
        twPlayGame.font = UIFont.boldSystemFont(ofSize: 24)
        twPlayGame.textAlignment = .left
        twPlayGame.textColor = UIColor.white
        twPlayGame.backgroundColor = UIColor.clear
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.playGameBtnClicked(_:)))
        playGameBtn.addGestureRecognizer(tap)
    }
    
    /*
     * Creates Category Button
     */
    private func createCategoryBtn(){
        
        chooseCategoryBtn.translatesAutoresizingMaskIntoConstraints = false
        chooseCategoryBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        chooseCategoryBtn.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 1.5).isActive = true
        chooseCategoryBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 10).isActive = true
        chooseCategoryBtn.topAnchor.constraint(equalTo: levelGameBtn.bottomAnchor,constant: view.frame.size.height/40).isActive = true
        chooseCategoryBtn.backgroundColor = .clear
        chooseCategoryBtn.layer.cornerRadius = 10
        chooseCategoryBtn.layer.borderWidth = 2
        chooseCategoryBtn.layer.borderColor = UIColor.white.cgColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.categoryBtnClicked(_:)))
        chooseCategoryBtn.addGestureRecognizer(tap)
        
        let categorySign = UIImageView(image:#imageLiteral(resourceName: "category_sign"))
        chooseCategoryBtn.addSubview(categorySign)
        categorySign.translatesAutoresizingMaskIntoConstraints = false
        categorySign.leftAnchor.constraint(equalTo: chooseCategoryBtn.leftAnchor, constant: self.view.frame.size.width / 24).isActive = true
        categorySign.widthAnchor.constraint(equalToConstant:self.view.frame.size.height  / 24).isActive = true
        categorySign.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 24).isActive = true
        categorySign.centerYAnchor.constraint(equalTo: chooseCategoryBtn.centerYAnchor,constant: 0).isActive = true

        let twCategory = UILabel()
        twCategory.text = "CATEGORY"
        chooseCategoryBtn.addSubview(twCategory)

        twCategory.translatesAutoresizingMaskIntoConstraints = false
        twCategory.leftAnchor.constraint(equalTo: categorySign.rightAnchor, constant: self.view.frame.size.width / 15).isActive = true
        twCategory.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2).isActive = true
        twCategory.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twCategory.centerYAnchor.constraint(equalTo: chooseCategoryBtn.centerYAnchor,constant: 0).isActive = true
        twCategory.font = UIFont.boldSystemFont(ofSize: 24)
        twCategory.textAlignment = .left
        twCategory.textColor = UIColor.white
        twCategory.backgroundColor = UIColor.clear
    }
    
    /*
     * Creates Level Game Button
     */
    private func createLevelGameBtn(){
        levelGameBtn.translatesAutoresizingMaskIntoConstraints = false
        levelGameBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        levelGameBtn.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 1.5).isActive = true
        levelGameBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 10).isActive = true
        levelGameBtn.topAnchor.constraint(equalTo: playGameBtn.bottomAnchor,constant: view.frame.size.height/40).isActive = true
        levelGameBtn.backgroundColor = .clear
        levelGameBtn.layer.cornerRadius = 10
        levelGameBtn.layer.borderWidth = 2
        levelGameBtn.layer.borderColor = UIColor.white.cgColor
        
        let levelGameSign = UIImageView(image:#imageLiteral(resourceName: "levels_sign"))
        levelGameBtn.addSubview(levelGameSign)
        levelGameSign.translatesAutoresizingMaskIntoConstraints = false
        levelGameSign.leftAnchor.constraint(equalTo: levelGameBtn.leftAnchor, constant: self.view.frame.size.width / 24).isActive = true
        levelGameSign.widthAnchor.constraint(equalToConstant:self.view.frame.size.height  / 24).isActive = true
        levelGameSign.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 24).isActive = true
        levelGameSign.centerYAnchor.constraint(equalTo: levelGameBtn.centerYAnchor,constant: 0).isActive = true
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.levelGameBtnClicked(_:)))
        levelGameBtn.addGestureRecognizer(tap)
        
        let twLevelGame = UILabel()
        twLevelGame.text = "LEVEL GAME"
        levelGameBtn.addSubview(twLevelGame)
        
        twLevelGame.translatesAutoresizingMaskIntoConstraints = false
        twLevelGame.leftAnchor.constraint(equalTo: levelGameSign.rightAnchor, constant: self.view.frame.size.width / 15).isActive = true
        twLevelGame.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2).isActive = true
        twLevelGame.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twLevelGame.centerYAnchor.constraint(equalTo: levelGameBtn.centerYAnchor,constant: 0).isActive = true
        twLevelGame.font = UIFont.boldSystemFont(ofSize: 24)
        twLevelGame.textAlignment = .left
        twLevelGame.textColor = UIColor.white
        twLevelGame.backgroundColor = UIColor.clear
        
        
    }
    
    /*
     * Creates UI Layout
     */
    private func createUiLayout(){
        
        iwAppLogo.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.size.height/10).isActive = true
        iwAppLogo.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iwAppLogo.widthAnchor.constraint(equalToConstant: iwAppLogo.frame.size.width/3).isActive = true
        iwAppLogo.heightAnchor.constraint(equalToConstant: iwAppLogo.frame.size.width/3).isActive = true
        
        var currentCategoryIndex = SharedPreferences.getIntValueForKey(key: SharedPreferences.CURRENT_CATEGORY)
        if(currentCategoryIndex == 0){
            currentCategoryIndex = 1
            SharedPreferences.setIntValueForKey(key: SharedPreferences.CURRENT_CATEGORY, value: 1)
        }
        
        twGameCategory.text = CategoryHelper.getCategoryFullNameTitleCase(category: currentCategoryIndex)
        twGameCategory.topAnchor.constraint(equalTo: iwAppLogo.bottomAnchor, constant: view.frame.size.height/40).isActive = true
        twGameCategory.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twGameCategory.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twGameCategory.heightAnchor.constraint(equalToConstant: view.frame.size.height/10).isActive = true
       
        twGameQuestion.text = CategoryHelper.getCategoryQuestion(category: currentCategoryIndex)
        twGameQuestion.topAnchor.constraint(equalTo: twGameCategory.bottomAnchor, constant: 0).isActive = true
        twGameQuestion.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twGameQuestion.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twGameQuestion.heightAnchor.constraint(equalToConstant: view.frame.size.height/30).isActive = true
        
        iwBackground.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        iwBackground.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        iwBackground.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        iwBackground.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

