//
//  ViewController.swift
//  HSNHL
//
//  Created by leobs on 04.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ViewController: AbstractViewController{
    
    let playerDao = PlayerDAO.init()
    var currentCategoryNumber = Int()
    var playerOne = Player(playerName: "", teamName: "")
    var playerTwo = Player(playerName: "", teamName: "")
    static var currentScore = 0
    var highScore = 0
    
    var isInLevelMode = false
    static var currentLevel = 2
    
    var isProcentualCategory = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(upBarView)
        view.addSubview(iwFirstPlayer)
        view.addSubview(iwSecondPlayer)
        
        currentCategoryNumber = SharedPreferences.getIntValueForKey(key: SharedPreferences.CURRENT_CATEGORY)
        
        playerOne = playerDao.getPlayerData(categoryToPlayNumber: currentCategoryNumber)
        playerTwo = playerDao.getPlayerData(categoryToPlayNumber: currentCategoryNumber)
        
        isInLevelMode = (SharedPreferences.getStringValueForKey(key: Strings.CURRENT_GAME_MODE) == Strings.LEVEL_GAME_MODE)
        if(isInLevelMode){
            twHighScore.text = "target : " + String(ViewController.currentLevel)
            currentCategoryNumber = ViewController.currentLevel % 7
        }
        
        isProcentualCategory = currentCategoryNumber == 4
        
        uiCreateUpBar()
        uiCreateFirstPlayerLayout()
        uiCreateSecondPlayerLayout()
        uiSetScore()
        loadAdMobBanner()
        
        setPlayerOneData(playerOne: playerOne)
        setPlayerTwoData(playerTwo: playerTwo)
        
    }

    /*
     * Sets player One Data
     */
    func setPlayerOneData(playerOne: Player){
        iwPlayerOneLogo.image = UIImage(named: TeamHelper.getTeamImage(teamName: "teamName"))
        iwPlayerOneLogo.isHidden = true
        twPlayerOneName.text = playerOne.playerName
        twPlayerOneTeam.text = "#"+TeamHelper.getTeamFullName(teamNameShort: playerOne.teamName!)
        if(isProcentualCategory){
            if(playerOne.playerCategoryStatToPlay! > 1){
                twPlayerOneStat.text = String(playerOne.playerCategoryStatToPlay!)+"%"
            } else {
                twPlayerOneStat.text = String(playerOne.playerCategoryStatToPlay!*100)+"%"
            }
            twHigher.text = "HIGHER"
            twLower.text = "LOWER"
        } else {
            twPlayerOneStat.text = String(playerOne.playerCategoryStatToPlay!)
            twHigher.text = "MORE"
            twLower.text = "LESS"
        }
        
        twPlayerOneVerb.text = "has"
        twPlayerOneCategory.text = CategoryHelper.getCategoryFullName(category: currentCategoryNumber )
    }

    
    /*
     * Sets player Two Data
     */
    func setPlayerTwoData(playerTwo: Player){
        iwPlayerTwoLogo.image = UIImage(named: TeamHelper.getTeamImage(teamName: "teamName"))
        iwPlayerTwoLogo.isHidden = true
        twPlayerTwoTeam.text = "#"+TeamHelper.getTeamFullName(teamNameShort: playerTwo.teamName!)
        twPlayerTwoName.text = playerTwo.playerName!
        twPlayerTwoStat.text = String(playerTwo.playerCategoryStatToPlay!)
        twPlayerTwoVerb.text = "has"
        twPlayerTwoCategory.text = CategoryHelper.getCategoryFullName(category: currentCategoryNumber )
        
    }
    
    @objc override func upDownListener(_ sender: UITapGestureRecognizer) {
        let yourDelay = 1
        fadeOut(view: self.iwVs,duration: 0.5)
        
        var userResult = false
        self.currentVsSign = "sign_wrong"
        
        var playerTwoCategoryStat = playerTwo.playerCategoryStatToPlay!
        let playerOneCategoryStat = playerOne.playerCategoryStatToPlay!
        
        if(isProcentualCategory &&
            (playerOneCategoryStat > 1 && playerTwoCategoryStat < 1)
            || (playerTwoCategoryStat > 1 && playerOneCategoryStat < 1)){
            playerTwoCategoryStat = playerTwoCategoryStat*100
        }
        
        if (sender.view == btnHigher){
            userResult = (playerTwoCategoryStat >= playerOneCategoryStat)
        } else if (sender.view == btnLower){
            userResult = (playerTwoCategoryStat <= playerOneCategoryStat)
        }
        
        
        self.fadeOut(view: self.btnHigher, duration: 0.3)
        self.fadeOut(view: self.btnLower, duration: 0.3)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(yourDelay), execute: { () -> Void in
            if(userResult){
                self.currentVsSign = "sign_right"
            }
            self.iwVs.image = UIImage(named: self.currentVsSign)
            self.fadeIn(view: self.iwVs, duration: 0.5)
        })
    
        
        
        let top = CGAffineTransform.init(translationX: 0, y: -self.view.frame.size.height/10)
        let down = CGAffineTransform.init(translationX: 0, y: 0)
        
        UIView.animate(withDuration: 1.5, delay: 0.0, options: [], animations: {
            self.twPlayerTwoCategory.transform = top
        }, completion: nil)
        
        twPlayerTwoStat.alpha = 1.0
        
        if(currentCategoryNumber == 4){
            twPlayerTwoStat.format = "%.1f%%"
            if(self.playerTwo.playerCategoryStatToPlay! > 100){
                twPlayerTwoStat.countFrom(0.0, to: CGFloat(self.playerTwo.playerCategoryStatToPlay!), withDuration: 1.5)
            } else {
                twPlayerTwoStat.countFrom(0.0, to: CGFloat(self.playerTwo.playerCategoryStatToPlay!*100), withDuration: 1.5)
            }
            
        } else {
            twPlayerTwoStat.format = "%.1f"
            twPlayerTwoStat.countFrom(0.0, to: CGFloat(self.playerTwo.playerCategoryStatToPlay!), withDuration: 1.5)
        }
        
        
        if(userResult){
        
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(4), execute: { () -> Void in
                if (self.isInLevelMode && (ViewController.currentScore + 1 == ViewController.currentLevel)) {
                    let levelCompleteViewController = self.storyboard?.instantiateViewController(withIdentifier: "LevelCompleteViewController") as! LevelCompleteViewController
                    LevelCompleteViewController.isLevelSuccessful = true
                    LevelCompleteViewController.levelPlayed = ViewController.currentLevel
                    SharedPreferences.setStringValueForKey(key: Strings.CURRENT_GAME_MODE, value: Strings.LEVEL_GAME_MODE)
                    self.present(levelCompleteViewController, animated: true, completion: nil)
                } else {
                    self.setPlayerOneData(playerOne: self.playerTwo)
                    self.playerOne = self.playerTwo
                    self.playerTwo = self.playerDao.getPlayerData(categoryToPlayNumber: self.currentCategoryNumber)
                    self.setPlayerTwoData(playerTwo: self.playerTwo)
                    
                    
                    self.fadeIn(view: self.twPlayerOneName, duration: 0.5)
                    self.fadeIn(view: self.twPlayerOneVerb, duration: 0.5)
                    self.fadeIn(view: self.twPlayerOneStat, duration: 0.5)
                    self.fadeIn(view: self.twPlayerOneCategory, duration: 0.5)
                    self.fadeIn(view: self.twPlayerOneTeam, duration: 0.5)
                    self.fadeIn(view: self.iwPlayerOneLogo, duration: 0.5)
                    self.fadeIn(view: self.iwVs, duration: 0.5)
                    
                    self.changeScore()
                    self.iwVs.image = UIImage(named: "vs_image")
                    self.twPlayerTwoCategory.transform = down
                    
                    self.fadeIn(view: self.twPlayerTwoName, duration: 0.5)
                    self.fadeIn(view: self.twPlayerTwoVerb, duration: 0.5)
                    self.fadeIn(view: self.btnHigher, duration: 0.5)
                    self.fadeIn(view: self.btnLower, duration: 0.5)
                    self.fadeIn(view: self.twPlayerTwoCategory, duration: 0.5)
                    self.fadeIn(view: self.twPlayerTwoTeam, duration: 0.5)
                    self.fadeIn(view: self.iwPlayerTwoLogo, duration: 0.5)
                }
                
            })
            
        } else {
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(3), execute: { () -> Void in
                if(self.isInLevelMode){
                    let levelCompleteViewController = self.storyboard?.instantiateViewController(withIdentifier: "LevelCompleteViewController") as! LevelCompleteViewController
                    LevelCompleteViewController.isLevelSuccessful = false
                    LevelCompleteViewController.levelPlayed = ViewController.currentLevel
                    SharedPreferences.setStringValueForKey(key: Strings.CURRENT_GAME_MODE, value: Strings.LEVEL_GAME_MODE)
                    self.present(levelCompleteViewController, animated: true, completion: nil)
                } else {
                    let endViewController = self.storyboard?.instantiateViewController(withIdentifier: "EndViewController") as! EndViewController
                    EndViewController.userScore = ViewController.currentScore
                    EndViewController.highScore = SharedPreferences.getIntValueForKey(key: SharedPreferences.HIGHSCORE+String(self.currentCategoryNumber))
                    EndViewController.worldScore = CategoryViewController.worldScores[self.currentCategoryNumber-1]
                    self.present(endViewController, animated: true, completion: nil)
                }
            })
            
        }
        
    }
    
    /*
     * Sets scores
     */
    func uiSetScore(){
        self.twScore.text = "score : " + String(ViewController.currentScore)
        if(!isInLevelMode){
            self.twHighScore.text = "highscore : " + String(SharedPreferences.getIntValueForKey(key: SharedPreferences.HIGHSCORE+String(currentCategoryNumber)))
        }
    }
    
    /*
     * Changes current Score
     */
    func changeScore(){
        ViewController.currentScore = ViewController.currentScore + 1
        self.twScore.text = "score : " + String(ViewController.currentScore)
        
        self.highScore = SharedPreferences.getIntValueForKey(key: SharedPreferences.HIGHSCORE+String(currentCategoryNumber))
        if(ViewController.currentScore > self.highScore){
            SharedPreferences.setIntValueForKey(key: SharedPreferences.HIGHSCORE + String(currentCategoryNumber), value: ViewController.currentScore)
            if(isInLevelMode){
                self.twHighScore.text = "target : "+String(ViewController.currentLevel)
            } else {
                self.twHighScore.text = "highscore : "+String(ViewController.currentScore)
            }
        }
        
    }
    
    /**
     * Fades in the UIView
     */
    func fadeIn(view: UIView, duration: Double) {
        UIView.animate(withDuration: duration, animations: { () -> Void in
            view.alpha = 1.0
        })
    }
    
    /**
     * Fades out the UIView
     */
    func fadeOut(view: UIView, duration: Double) {
        UIView.animate(withDuration: duration, animations: { () -> Void in
            view.alpha = 0.0
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    

}



