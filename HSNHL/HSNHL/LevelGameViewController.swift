//
//  LevelGameViewController.swift
//  HSNBA
//
//  Created by leobs on 07.04.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation
import UIKit

class LevelGameViewController:UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
    
    
    let array = ["league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all","league_all"]
    
    let upBarView : UIView = {
        let upBarView = UIView()
        upBarView.translatesAutoresizingMaskIntoConstraints = false
        return upBarView
    }()
    
    let twLevels : UILabel = {
        let twLevels = UILabel()
        twLevels.translatesAutoresizingMaskIntoConstraints = false
        twLevels.text = "Levels"
        twLevels.backgroundColor = .clear
        twLevels.font = UIFont.boldSystemFont(ofSize: 20)
        twLevels.textColor = .white
        twLevels.textAlignment = .center
        return twLevels
    }()
    
    let twBackBtn : UILabel = {
        let twBackBtn = UILabel()
        twBackBtn.translatesAutoresizingMaskIntoConstraints = false
        twBackBtn.text = "Back"
        twBackBtn.backgroundColor = .clear
        twBackBtn.font = UIFont.boldSystemFont(ofSize: 14)
        twBackBtn.textColor = .white
        twBackBtn.textAlignment = .left
        return twBackBtn
    }()
    
    var gameToPlay = 0
    var levelCellVOs : Array<LevelCellVO> = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let collectionView = createCollectionView()
        self.view.addSubview(upBarView)
        self.view.addSubview(collectionView)
        createUpBarView()

        gameToPlay = SharedPreferences.getIntValueForKey(key: Strings.MAX_LEVEL)
        if(gameToPlay == 0){ gameToPlay = 1}
        
        for index in 1...82 {
            if(index < gameToPlay){
                let levelCellVO = LevelCellVO(levelNumber: index, levelPlay: "", levelIcon: "ic_green_tick", isLevelHidden: true)
             levelCellVOs.append(levelCellVO)
            } else if(index == gameToPlay){
                let levelCellVO = LevelCellVO(levelNumber: index,levelPlay: "PLAY", levelIcon: "ic_green_play", isLevelHidden: true)
             levelCellVOs.append(levelCellVO)
            } else {
                let levelCellVO = LevelCellVO(levelNumber: index, levelPlay: "", levelIcon: "ic_blue_lock", isLevelHidden: false)
             levelCellVOs.append(levelCellVO)
            }
        }
        
        collectionView.topAnchor.constraint(equalTo: upBarView.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true

    }
    
    @objc func backBtnClicked(_ sender: UITapGestureRecognizer){
        let touchPoint = sender.location(in: self.view)
        let backBtnClicked = (touchPoint.x < self.view.frame.width/4)
        if(backBtnClicked){
            let startViewController = self.storyboard?.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
            self.present(startViewController, animated: true, completion: nil)
        }
    }
    
    /*
     * Creates Up Bar View
     */
    func createUpBarView(){
        upBarView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        upBarView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        upBarView.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        upBarView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        upBarView.backgroundColor = UIColor(red:15/255, green: 117/255, blue: 189/255,alpha: 1.0)
        
        upBarView.addSubview(twLevels)
        twLevels.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twLevels.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        twLevels.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twLevels.bottomAnchor.constraint(equalTo: upBarView.bottomAnchor).isActive = true
        
        upBarView.addSubview(twBackBtn)
        twBackBtn.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twBackBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        twBackBtn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: self.view.frame.size.width/30).isActive = true
        twBackBtn.bottomAnchor.constraint(equalTo: upBarView.bottomAnchor).isActive = true
        
        upBarView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backBtnClicked(_:)))
        twBackBtn.isUserInteractionEnabled = true
        twBackBtn.addGestureRecognizer(tap)
    }
    
    /*
     * Creates Collection View
     */
    func createCollectionView() -> UICollectionView {
        let itemSize = UIScreen.main.bounds.width/3 - 3
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(5, 0, 10, 0)
        layout.itemSize = CGSize(width: itemSize, height:itemSize)
        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 5
        
        let collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(LevelCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.backgroundView =  UIImageView(image: #imageLiteral(resourceName: "main_page"))
        return collectionView
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 81
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! LevelCell
        cell.twLevelNumber.text = String(levelCellVOs[indexPath.row].levelNumber)
        cell.iwLevelSign.image = UIImage(named:levelCellVOs[indexPath.row].levelIcon)
        cell.iwCover.isHidden = levelCellVOs[indexPath.row].isLevelHidden
        cell.twPlaySign.text = levelCellVOs[indexPath.row].levelPlay
        
         return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(gameToPlay)
        print(indexPath.row + 1)
        
        if ((indexPath.row + 1) <= gameToPlay){
            ViewController.currentLevel = indexPath.row + 1
            goToNextController()
        }
    }
    
    
    /*
     * Goes to next controller
     */
   func goToNextController(){
        let compareViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        SharedPreferences.setStringValueForKey(key: Strings.CURRENT_GAME_MODE, value: Strings.LEVEL_GAME_MODE)
        ViewController.currentScore = 0
        LevelCompleteViewController.startTime = NSDate()
        self.present(compareViewController, animated: true, completion: nil)
    }
    
    
}
