//
//  LevelCellDAO.swift
//  HSNBA
//
//  Created by leobs on 10.04.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class LevelCellVO{
    
    var levelNumber : Int
    var levelPlay : String
    var isLevelHidden : Bool
    var levelIcon : String
    
    init(levelNumber: Int, levelPlay: String, levelIcon: String, isLevelHidden: Bool){
        self.levelNumber = levelNumber
        self.levelPlay = levelPlay
        self.levelIcon = levelIcon
        self.isLevelHidden = isLevelHidden
    }
    
}
